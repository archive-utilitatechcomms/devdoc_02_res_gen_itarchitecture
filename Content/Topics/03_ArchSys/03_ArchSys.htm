﻿<?xml version="1.0" encoding="utf-8"?>
<html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd">
    <head>
    </head>
    <body>
        <h1>System Architecture Layer</h1>
        <p>System (software) architecture refers to the fundamental structures of a software system and the discipline of creating such structures and systems. Each structure comprises software elements, relations among them, and properties of both elements and relations. The architecture of a software system is a metaphor, analogous to the architecture of a building. It functions as a blueprint for the system and the developing project, laying out the tasks necessary to be executed by the design teams.</p>
        <p>Software architecture is about making fundamental structural choices that are costly to change once implemented. Software architecture choices include specific structural options from possibilities in the design of the software. Therefore, an appropriate real-time computing language would need to be chosen. Additionally, to satisfy the need for reliability the choice could be made to have multiple redundant and independently produced copies of the program, and to run these copies on independent hardware while cross-checking results.</p>
        <p>Documenting software architecture facilitates communication between stakeholders, captures early decisions about the high-level design, and allows reuse of design components between projects.</p>
        <p>The System Architecture layer consists of the following sub layers:</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Application_Architecture_Layer"></a>Application Architecture Sub Layer</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>This is about software applications that support the components in the business with application services – focusing on front-end/UI.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="API_Architecture_Layer"></a>API Architecture Sub Layer</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>This is about software applications that support the components in the business with application services – focusing on API services.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot><a name="Data_Architecture_Layer"></a>Data Architecture Sub Layer</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>This is about software applications that support the components in the business with application services – focusing on data and databases.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>We use the following models to map our systems:</p>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>C4 Model</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The <a href="https://c4model.com/" target="_blank" title="The C4 model for visualising software architecture." alt="The C4 model for visualising software architecture.">C4 model</a> is a lean graphical notation technique for modelling the architecture of software systems.  It is based on a structural decomposition of a system into containers and components and relies on existing modelling techniques such as the Unified Modelling Language (UML) or Entity Relation Diagrams (ERD) for the more detailed decomposition of the architectural building blocks.</p>
                <p>The C4 model doesn't prescribe any particular notation, and what you see in these sample diagrams is a simple notation that works well on whiteboards, paper, sticky notes, index cards, and a variety of diagramming tools. You can use UML as your notation, too, with the appropriate use of packages, components, and stereotypes. Regardless of the notation that you use, I would recommend that every element includes a name, the element type (i.e., "Person", "Software System", "Container", or "Component"), a technology choice (if appropriate), and some descriptive text. It might seem unusual to include so much text in a diagram, but this additional text removes much of ambiguity typically seen on software architecture diagrams.</p>
                <p>Make sure that you have a key/legend to describe any notation that you're using, even if it's obvious to you. This should cover colours, shapes, acronyms, line styles, borders, sizing, etc. Your notation should ideally remain consistent across each level of detail. Here is the diagram key/legend for the container diagram shown previously.</p>
                <p>
                    <img src="IMG_C4_00.jpg" class="Screen_Thumbnail" />
                </p>
                <p>C4 model documents the architecture of a software system, by showing multiple point of views that explain the decomposition of a system into containers and components, the relationship between these elements, and, where appropriate, the relation with its users.</p>
                <p>The viewpoints are organised according to their hierarchical level:</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Level 1 - Context diagrams</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Shows the system in scope and its relationship with users and other systems. This level is reflected as a system context diagram, which shows the software system you are building and how it fits into the world in terms of the people who use it and the other software systems it interacts with.</p>
                        <p>Here is an example of a system context diagram that describes an Internet banking system that you may be building:</p>
                        <p>
                            <img src="IMG_C4_01.jpg" class="Screen_Thumbnail" />
                        </p>
                        <p>In this example, personal customers of the bank use the Internet banking system to view information about their bank accounts and to make payments. The Internet banking system uses the bank's existing mainframe banking system to do this, and uses the bank's existing e-mail system to send e-mail to customers. Colour coding in the diagram indicates which software systems already exist (the grey boxes) and those to be built (blue).</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Level 2 - Container diagrams  </MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Decomposes a system into interrelated containers.  A container is an executable and deployable sub-system. This level reflects a container diagram, and zooms into the software system, and shows the containers (applications, data stores, microservices, etc.) that make up that software system. Technology decisions are also a key part of this diagram.</p>
                        <p>Below is a sample container diagram for the Internet banking system. It shows that the Internet banking system (the dashed box) is made up of five containers: a server-side web application, a client-side single-page application, a mobile app, a server-side API application, and a database.</p>
                        <p>
                            <img src="IMG_C4_02.jpg" class="Screen_Thumbnail" />
                        </p>
                        <p>The web application is a Java/Spring MVC web application that simply serves static content (HTML, CSS, and JavaScript), including the content that makes up the single-page application. The single-page application is an Angular application that runs in the customer's web browser, providing all of the Internet banking features. Alternatively, customers can use the cross-platform Xamarin mobile app to access a subset of the Internet banking functionality. Both the single-page application and mobile app use a JSON/HTTPS API, which another Java/Spring MVC application running on the server side provides. The API application gets user information from the database (a relational-database schema). The API application also communicates with the existing mainframe banking system, using a proprietary XML/HTTPS interface, to get information about bank accounts or make transactions. The API application also uses the existing e-mail system if it needs to send e-mail to customers.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Level 3 - Component diagrams</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Decomposes containers into interrelated components, and relate the components to other containers or other systems. This level reflects a component diagram, zooms into an individual container to show the components inside it. These components should map to real abstractions (e.g., a grouping of code) in your codebase.</p>
                        <p>Here is a sample component diagram for the fictional Internet banking system that shows some (rather than all) of the components within the API application.</p>
                        <p>
                            <img src="IMG_C4_03.jpg" class="Screen_Thumbnail" />
                        </p>
                        <p>Two Spring MVC Rest Controllers provide access points for the JSON/HTTPS API, with each controller subsequently using other components to access data from the database and mainframe banking system.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Level 4 - Code diagrams</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>Provides additional details about the design of the architectural elements that can be mapped to code.  C4 model relies at this level on existing notations such as Unified Modelling Language (UML), Entity Relation Diagrams (ERD) or diagrams generated by Integrated Development Environments (IDE).</p>
                        <p>If you really want or need to, you can zoom into an individual component to show how that component is implemented. This is a sample (and partial) UML class diagram for the fictional Internet banking system that, showing the code elements (interfaces and classes) that make up the MainframeBankingSystemFacade component.</p>
                        <p>
                            <img src="IMG_C4_04.jpg" class="Screen_Thumbnail" />
                        </p>
                        <p>It shows that the component is made up of a number of classes, with the implementation details directly reflecting the code. I wouldn't necessarily recommend creating diagrams at this level of detail, especially when you can obtain them on demand from most IDEs.</p>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>For level 1 to 3, the C4 model use 5 basic diagramming elements: persons, software systems, containers, components and relationships. The technique is not prescriptive for the layout, shape, colour and style of these elements.  Instead, C4 model recommends to use simple diagrams based on nested boxes in order to facilitate interactive collaborative drawing. The technique also promotes good modelling practices such as providing on every diagram a title and a legend, and clear unambiguous labelling in order to facilitate the understanding by the intended audience.</p>
                <p>C4 model facilitates collaborative visual architecting and evolutionary architecture in the context of agile teams where more formal documentation methods and up-front architectural design are not desired.</p>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <MadCap:dropDown>
            <MadCap:dropDownHead>
                <MadCap:dropDownHotspot>Unified Modeling Language (UML)</MadCap:dropDownHotspot>
            </MadCap:dropDownHead>
            <MadCap:dropDownBody>
                <p>The <a href="http://uml.org/" target="_blank" title="Unified Modeling Language (UML)" alt="Unified Modeling Language (UML)">Unified Modeling Language (UML)</a> is a general-purpose, developmental, modeling language in the field of software engineering that is intended to provide a standard way to visualise the design of a system. </p>
                <p>UML offers a way to visualise a system's architectural blueprints in a diagram, including elements such as:</p>
                <ul>
                    <li>Any activities (jobs).</li>
                    <li>Individual components of the system.</li>
                    <li>How each component can interact with other software components.</li>
                    <li>How the system will run.</li>
                    <li>How entities interact with others (components and interfaces).</li>
                    <li>External user interface.</li>
                </ul>
                <p>Although originally intended for object-oriented design documentation, UML has been extended to a larger set of design documentation. </p>
                <p>The <a href="https://www.omg.org/" target="_blank" title="Object Management Group (OMG)" alt="Object Management Group (OMG)">Object Management Group (OMG)</a> has developed a metamodeling architecture to define the UML, called the Meta-Object Facility. MOF is designed as a four-layered architecture:</p>
                <p>
                    <img src="IMG_UML_00A.png" class="Screen_Thumbnail" />
                </p>
                <p>It provides a meta-meta model at the top, called the M3 layer. This M3-model is the language used by Meta-Object Facility to build metamodels, called M2-models.</p>
                <p>The most prominent example of a Layer 2 Meta-Object Facility model is the UML metamodel, which describes the UML itself. These M2-models describe elements of the M1-layer, and thus M1-models. These would be, for example, models written in UML. The last layer is the M0-layer or data layer. It is used to describe runtime instances of the system.</p>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Modeling</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>It is important to distinguish between the UML model and the set of diagrams of a system. A diagram is a partial graphic representation of a system's model. The set of diagrams need not completely cover the model and deleting a diagram does not change the model. The model may also contain documentation that drives the model elements and diagrams (such as written use cases).</p>
                        <p>UML diagrams represent two different views of a system model:</p>
                        <ul>
                            <li><b>Static (or structural) view</b>: Emphasises the static structure of the system using objects, attributes, operations and relationships. It includes class diagrams and composite structure diagrams.</li>
                            <li><b>Dynamic (or behavioural) view</b>: Emphasises the dynamic behaviour of the system by showing collaborations among objects and changes to the internal states of objects. This view includes sequence diagrams, activity diagrams and state machine diagrams.</li>
                        </ul>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <MadCap:dropDown>
                    <MadCap:dropDownHead>
                        <MadCap:dropDownHotspot>Diagrams</MadCap:dropDownHotspot>
                    </MadCap:dropDownHead>
                    <MadCap:dropDownBody>
                        <p>UML has many types of diagrams, which are divided into various categories. These diagrams can be categorised hierarchically as shown in the following class diagram:</p>
                        <p>
                            <img src="IMG_UML_00.png" class="Screen_Thumbnail" />
                        </p>
                        <p>Some types represent structural information, and the rest represent general types of behaviour, including a few that represent different aspects of interactions. </p>
                        <MadCap:dropDown>
                            <MadCap:dropDownHead>
                                <MadCap:dropDownHotspot>Structure diagrams</MadCap:dropDownHotspot>
                            </MadCap:dropDownHead>
                            <MadCap:dropDownBody>
                                <p>Structure diagrams emphasize the things that must be present in the system being modeled. </p>
                                <p>
                                    <img src="IMG_UML_01A.png" class="Screen_Thumbnail" />
                                </p>
                                <p>Since structure diagrams represent the structure, they are used extensively in documenting the software architecture of software systems. For example, the component diagram describes how a software system is split up into components and shows the dependencies among these components.</p>
                                <p>
                                    <img src="IMG_UML_01B.png" class="Screen_Thumbnail" />
                                </p>
                                <p>&#160;</p>
                            </MadCap:dropDownBody>
                        </MadCap:dropDown>
                        <MadCap:dropDown>
                            <MadCap:dropDownHead>
                                <MadCap:dropDownHotspot>Behaviour diagrams</MadCap:dropDownHotspot>
                            </MadCap:dropDownHead>
                            <MadCap:dropDownBody>
                                <p>Behaviour diagrams emphasise what must happen in the system being modeled. </p>
                                <p>
                                    <img src="IMG_UML_02A.png" class="Screen_Thumbnail" />
                                </p>
                                <p>Since behaviour diagrams illustrate the behaviour of a system, they are used extensively to describe the functionality of software systems. As an example, the activity diagram describes the business and operational step-by-step activities of the components in a system.</p>
                                <p>
                                    <img src="IMG_UML_02B.png" class="Screen_Thumbnail" />
                                </p>
                                <p>&#160;</p>
                            </MadCap:dropDownBody>
                        </MadCap:dropDown>
                        <MadCap:dropDown>
                            <MadCap:dropDownHead>
                                <MadCap:dropDownHotspot>Interaction diagrams</MadCap:dropDownHotspot>
                            </MadCap:dropDownHead>
                            <MadCap:dropDownBody>
                                <p>Interaction diagrams, a subset of behaviour diagrams, emphasise the flow of control and data among the things in the system being modeled. </p>
                                <p>
                                    <img src="IMG_UML_03A.png" class="Screen_Thumbnail" />
                                </p>
                                <p>For example, the sequence diagram shows how objects communicate with each other regarding a sequence of messages.</p>
                                <p>
                                    <img src="IMG_UML_03B.png" class="Screen_Thumbnail" />
                                </p>
                                <p>&#160;</p>
                            </MadCap:dropDownBody>
                        </MadCap:dropDown>
                        <p>&#160;</p>
                    </MadCap:dropDownBody>
                </MadCap:dropDown>
                <p>&#160;</p>
            </MadCap:dropDownBody>
        </MadCap:dropDown>
        <p>&#160;</p>
        <p>&#160;</p>
        <p>&#160;</p>
    </body>
</html>