﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent id="commercial-sales-application">
        <MicroContentPhrase>Commercial Sales&#160;Application</MicroContentPhrase>
        <MicroContentPhrase>CSA</MicroContentPhrase>
        <MicroContentPhrase>Commercial Sales Reporting</MicroContentPhrase>
        <MicroContentPhrase>Commercial Operations</MicroContentPhrase>
        <MicroContentPhrase>Customer Service Advisor</MicroContentPhrase>
        <MicroContentPhrase>Commercial Sales App</MicroContentPhrase>
        <MicroContentPhrase>Business Sales Portal</MicroContentPhrase>
        <MicroContentPhrase>Broker Portal</MicroContentPhrase>
        <MicroContentPhrase>BSP</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Application: Business Sales Portal</xhtml:h1>
                    <xhtml:p>At Utilita Energy, we use brokers to provide us with the vast majority of our commercial electricity and gas customers. When we take on a customer that was provided by a broker, and where the broker has specified that an uplift is to be applied, we pay the broker an agreed commission. The  <xhtml:a xhtml:href="https://brokers.utilita.co.uk/" xhtml:target="_blank" xhtml:title="https://brokers.utilita.co.uk/" xhtml:alt="https://brokers.utilita.co.uk/">Business Sales Portal</xhtml:a> website allows users to keep track of and manage these sales broker payments and commissions. It is used extensively by Commercial Operations at our Colchester office and also at Hutwood Court.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Business Sales Portal End- User Help Guides</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Depending on if you work internally for Utilita, externally to Utilita as a broker, or if you manage Business Sales Portal settings in Utilita HQ, there's three end-user guides associated with the Business Sales Portal, as follows:</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                            <xhtml:div xhtml:class="RespLayoutNewRowClass1">
                                <xhtml:div>
                                    <xhtml:p><xhtml:a xhtml:href="http://ugl-jir-001-dev/TechPubs/Help_BSP_Internal" xhtml:target="_blank" xhtml:title="Business Sales Portal (Internal) End-User Help Guide. For Utilita employees." xhtml:alt="Business Sales Portal (Internal) End-User Help Guide. For Utilita employees."><xhtml:img xhtml:src="../../Resources/Images/00_TechPubs_Front/IMG_TechPubs_BSP_Int.png" xhtml:style="width: 360px;min-width: 360px;max-width: 360px;height: 113px;min-height: 113px;max-height: 113px;" /></xhtml:a>
                                    </xhtml:p>
                                </xhtml:div>
                                <xhtml:div>
                                    <xhtml:p><xhtml:a xhtml:href="http://ugl-jir-001-dev/TechPubs/Help_BSP_External" xhtml:target="_blank" xhtml:title="Business Sales Portal (External) End-User Help Guide. For external brokers." xhtml:alt="Business Sales Portal (External) End-User Help Guide. For external brokers."><xhtml:img xhtml:src="../../Resources/Images/00_TechPubs_Front/IMG_TechPubs_BSP_Ext.png" xhtml:style="width: 360px;min-width: 360px;max-width: 360px;height: 113px;min-height: 113px;max-height: 113px;" /></xhtml:a>
                                    </xhtml:p>
                                </xhtml:div>
                                <xhtml:div>
                                    <xhtml:p><xhtml:a xhtml:href="http://ugl-jir-001-dev/TechPubs/Help_UHQ" xhtml:target="_blank" xhtml:title="Utilita HQ End-User Help Guide" xhtml:alt="Utilita HQ End-User Help Guide"><xhtml:img xhtml:src="../../Resources/Images/00_TechPubs_Front/IMG_TechPubs_UHQ_Ext.png" xhtml:style="width: 360px;min-width: 360px;max-width: 360px;height: 113px;min-height: 113px;max-height: 113px;" /></xhtml:a>
                                    </xhtml:p>
                                </xhtml:div>
                            </xhtml:div>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Business Sales Portal DevOps Help Guide</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>You can find further low-level business processes and technical information about the Business Sales Portal in the following link:</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                            <xhtml:div xhtml:class="RespLayoutNewRowClass1">
                                <xhtml:div>
                                    <xhtml:p><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_WebMob_LL_broker.utilita.co.uk_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Business Sales Portal DevOps Help Guide" xhtml:alt="Business Sales Portal DevOps Help Guide"><xhtml:img xhtml:src="../../Resources/Images/00_Portal_System/IMG_Sys_01f_HelpDevOps.png" xhtml:style="width: 360px;min-width: 360px;max-width: 360px;height: 113px;min-height: 113px;max-height: 113px;" /></xhtml:a>
                                    </xhtml:p>
                                </xhtml:div>
                                <xhtml:div>
                                    <xhtml:p>&#160;</xhtml:p>
                                </xhtml:div>
                                <xhtml:div>
                                    <xhtml:p>&#160;</xhtml:p>
                                </xhtml:div>
                            </xhtml:div>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>&#160;</xhtml:p>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">The Business Sales Portal has also been called the Commercial Sales Application (CSA) and the Broker Portal. </xhtml:p>
                    <xhtml:p>&#160;</xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>