﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Postman</MicroContentPhrase>
        <MicroContentPhrase>Postman Desktop</MicroContentPhrase>
        <MicroContentPhrase>Postman Browser</MicroContentPhrase>
        <MicroContentPhrase>API reference</MicroContentPhrase>
        <MicroContentPhrase>API references</MicroContentPhrase>
        <MicroContentPhrase>API Testing</MicroContentPhrase>
        <MicroContentPhrase>Testing APIs</MicroContentPhrase>
        <MicroContentPhrase>Integrated Developer Environment</MicroContentPhrase>
        <MicroContentPhrase>IDE</MicroContentPhrase>
        <MicroContentPhrase>REST</MicroContentPhrase>
        <MicroContentPhrase>API</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:head>
                    <xhtml:link xhtml:href="../../Resources/TableStyles/TabMenuMap.css" xhtml:rel="stylesheet" MadCap:stylesheetType="table" />
                </xhtml:head>
                <xhtml:body>
                    <xhtml:h1>Tool: Postman (API &amp; Integrated Developer Environment - IDE)</xhtml:h1>
                    <xhtml:p><xhtml:a xhtml:href="https://www.getpostman.com/products" xhtml:target="_blank" xhtml:title="Postman " xhtml:alt="Postman ">Postman</xhtml:a> provides API references and executable descriptions of our simple APIs. Individual development teams use shared workspaces for our API projects to support ongoing development and encourage collaboration. Postman is a tool used by API developers and test analysts to set up and run test cases using APIs. It consists of a desktop application and a browser-based tool:</xhtml:p>
                    <xhtml:table xhtml:style="margin-left: 0;margin-right: auto;mc-table-style: url('../../Resources/TableStyles/TabMenuMap.css');" xhtml:class="TableStyle-TabMenuMap" xhtml:cellspacing="0">
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" xhtml:style="width: 50px;" />
                        <xhtml:col xhtml:class="TableStyle-TabMenuMap-Column-Column1" xhtml:style="width: 500px;" />
                        <xhtml:thead>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Head-Header1">
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadE-Column1-Header1">Platform</xhtml:th>
                                <xhtml:th xhtml:class="TableStyle-TabMenuMap-HeadD-Column1-Header1">Description</xhtml:th>
                            </xhtml:tr>
                        </xhtml:thead>
                        <xhtml:tbody>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyE-Column1-Body1">Postman desktop</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyD-Column1-Body1">
                                    <xhtml:ul>
                                        <xhtml:li>Allows test cases to be created and run.</xhtml:li>
                                        <xhtml:li>Allows API reference documentation to be written (using  Markdown).</xhtml:li>
                                    </xhtml:ul>
                                </xhtml:td>
                            </xhtml:tr>
                            <xhtml:tr xhtml:class="TableStyle-TabMenuMap-Body-Body1">
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyB-Column1-Body1">Browser-based tool</xhtml:td>
                                <xhtml:td xhtml:class="TableStyle-TabMenuMap-BodyA-Column1-Body1">
                                    <xhtml:ul>
                                        <xhtml:li>Best environment for viewing API references.</xhtml:li>
                                        <xhtml:li>Features the account management tools</xhtml:li>
                                    </xhtml:ul>
                                </xhtml:td>
                            </xhtml:tr>
                        </xhtml:tbody>
                    </xhtml:table>
                    <xhtml:p>You can find further information about Postman by clicking on the following links:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_LL_API_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="API&#160;DevOps Help Guide" xhtml:alt="API&#160;DevOps Help Guide" xhtml:target="_blank">API&#160;DevOps Help Guide</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_01_Qual_Gen_API-Ref_GDE/Content/Topics/07_Postman/07_Postman.htm" xhtml:title="Documenting APIs in Postman" xhtml:alt="Documenting APIs in Postman" xhtml:target="_blank">Documenting APIs in Postman</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="https://utiliadevelopment.postman.co/workspaces/a73f5e79-92cc-4d06-867e-d02fe411cdd3/collections" xhtml:target="_blank" xhtml:title="Postman: Utilita Energy Oracle API Documentation" xhtml:alt="Postman: Utilita Energy Oracle API Documentation">Postman: API References</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>To edit the documentation and run test cases, you must use the Postman desktop application. You will need to:<xhtml:br /></xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>Install the Postman desktop app (download it <xhtml:a xhtml:href="https://www.getpostman.com/downloads/" xhtml:target="_blank" xhtml:title="Get Postman for Windows" xhtml:alt="Get Postman for Windows">here</xhtml:a>).</xhtml:li>
                    </xhtml:ul>
                    <xhtml:ul>
                        <xhtml:li>Set up a Postman account and request the appropriate permissions. Please contact an IT Development Manager or the Technical Communications Manager, who can set up an account for you. </xhtml:li>
                    </xhtml:ul>
                    <xhtml:h2>Postman Functionality</xhtml:h2>
                    <xhtml:p>Every tool within Postman is based from the Postman Collection format - mocks, testing, documentation, monitoring, and publishing all begin with a Postman Collection. Postman is used to:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li>Design and mock APIs, Create API specifications directly within Postman’s Collection format. Support split-stack development with Postman’s mock service, enabling frontend and backend developers to work in parallel.</xhtml:li>
                        <xhtml:li>Debug APIs with tests, scripts, variables, and more. Send requests to verify that our API is ready for launch. Include pre-request scripts, tests, and code snippets to make debugging more powerful. Examine our API’s responses directly within Postman. Use variables and environments to save and reuse values in multiple places. Save everything into a comprehensive collection.</xhtml:li>
                        <xhtml:li>Automated testing with Postman, and integrated into our <xhtml:a xhtml:href="https://www.getpostman.com/docs/v6/postman/collection_runs/integration_with_jenkins" xhtml:target="_blank" xhtml:title="Integration with Jenkins" xhtml:alt="Integration with Jenkins">Jenkins Continuous Integration and Delivery (CI/CD) pipeline</xhtml:a>. Collect the tests and requests we’ve created into a single automated test sequence with the Collection Runner. Use <xhtml:a xhtml:href="https://www.getpostman.com/docs/v6/postman/collection_runs/command_line_integration_with_newman" xhtml:target="_blank" xhtml:title="Command line integration with Newman" xhtml:alt="Command line integration with Newman">Newman</xhtml:a> or <xhtml:a xhtml:href="https://jenkins.io/" xhtml:target="_blank" xhtml:title="Jenkins" xhtml:alt="Jenkins">Jenkins</xhtml:a> to integrate Postman Collections into an existing CI/CD process.</xhtml:li>
                        <xhtml:li>Monitor APIs for more than just uptime - test for correctness and responsiveness. Stay in the loop with our mission-critical APIs by setting monitors to run at our preferred cadence. Test for performance as well as behaver - verify that the API is both responding and working as expected. Review Monitoring results within our Postman Dashboard.</xhtml:li>
                        <xhtml:li>Publish and onboard developers to our APIs with Collections and Documentation. Feature a Run in Postman button on our site or GitHub repository, allowing developers to easily download our APsI directly into their Postman instance. List our APIs in the Postman API Network - the most complete and actionable listing of APIs, accessible via the web as well as directly through the Postman App.</xhtml:li>
                        <xhtml:li>Create detailed, web-viewable <xhtml:a xhtml:href="https://www.getpostman.com/docs/v6/postman/api_documentation/intro_to_api_documentation" xhtml:target="_blank" xhtml:title="Postman API documentation" xhtml:alt="Postman API documentation">documentation</xhtml:a> for our APIs, set to private. Help developers onboard to our APIs by downloading the reference documentation directly into their Postman instance. Enrich documentation by including tests, examples, descriptions, and code snippets.</xhtml:li>
                    </xhtml:ul>
                    <xhtml:h2>Postman Support and Documentation</xhtml:h2>
                    <xhtml:ul>
                        <xhtml:li>Click <xhtml:a xhtml:href="https://utiliadevelopment.postman.co/workspaces/ca032533-2d17-4559-a3fc-06b418430666/collections" xhtml:target="_blank" xhtml:title="Utilita Development Team Postman Workspace" xhtml:alt="Utilita Development Team Postman Workspace">here</xhtml:a> to access the Utilita Development Team Postman Workspace.</xhtml:li>
                        <xhtml:li>For Postman licensing and support, contact Utilita's <xhtml:a xhtml:href="mailto:techpubs@utilita.co.uk?subject=MadCap Flare licensing and support" xhtml:title="Technical Communications team (techpubs@utilita.co.uk)" xhtml:alt="Technical Communications team (techpubs@utilita.co.uk)">Technical Communications team</xhtml:a>.</xhtml:li>
                        <xhtml:li>Click <xhtml:a xhtml:href="https://www.getpostman.com/docs/v6/" xhtml:target="_blank" xhtml:title="Postman support and documentation." xhtml:alt="Postman support and documentation.">here</xhtml:a> for Postman support and documentation.</xhtml:li>
                    </xhtml:ul>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>