﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Meter Dialler</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Database: Meter Dialler</xhtml:h1>
                    <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">For information on Utilita Energy's wider database infrastructure, see <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_02_Res_Gen_System_Infrastructure/Content/Topics/03_Database_Architecture/03a_Database_Architecture.htm" xhtml:target="_blank" xhtml:title="Development Resources: System Infrastructure (Architecture - Databases)" xhtml:alt="Development Resources: System Infrastructure (Architecture - Databases)">database architecture</xhtml:a>.</xhtml:p>
                    <xhtml:p>The Meter Dialler database is essentially a work queue of message requests to contact the meters installed in the field to obtain up to date reading information. In <xhtml:a xhtml:href="http://www.orafaq.com/wiki/Tnsnames.ora" xhtml:target="_blank" xhtml:title="Welcome to The Oracle FAQ (Tnsnames.ora)" xhtml:alt="Welcome to The Oracle FAQ (Tnsnames.ora)">tnsnames.ora</xhtml:a> files, it is commonly named MTRDIAL.</xhtml:p>
                    <xhtml:p>Utilita currently has around 30,000 SMETS1 smart meters installed through the UK, each of which contains a SIM card which Meter Dialler uses to communicate with. Newer meters accept connections over the 3G network and so are contacted by SMS rather than being called using a modem.</xhtml:p>
                    <xhtml:p>Older meters use GSM, but connection issues can occur with modem dialup, which is part of the reason for the move towards smart meters. Regardless of the age of the meter or the communication technology, readings are taken from each meter on a monthly or twice monthly basis to pull data which is then stored in the Meter Dialler database.</xhtml:p>
                    <xhtml:p>The Meter Dialler database maintains work queues for modem dialup, SMS, and email communication with the appropriate meter population. As well as holding the requests to contact each meter, the delivery status of the messages are also written to the database. The SIM cards in the meters are from a range of mobile providers. The modems triggered by the Meter Dialler call queue are differently configured per SIM card provider, so specific O2 modems will contact O2 SIMs, Vodafone modems will contact Vodafone SIMs, and so on.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Meter Dialler App</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Meter Dialler app is a messaging interface for first generation smart meters (non-industry standard). It uses modems to contact the meter population in the field. </xhtml:p>
                            <xhtml:p>Refer to <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_DB_HL_MD_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Meter Dialler Database DevOps Help Guide" xhtml:alt="Meter Dialler Database DevOps Help Guide">Meter Dialler Database DevOps Help Guide</xhtml:a> for more information.</xhtml:p>
                            <xhtml:p>Written in Delphi v7.0 using additional freeware libraries (Ciacomport) to activate the 'Comms' ports, the application dials the SIM phone numbers in the meters based on the information in the CRM Meter Dialler database. The Comms ports, modems and SIMs are tied / hardwired to each server. Secure is responsible for the SIMs used to dial meters on the Isle of Man which generate much higher data tariffs.</xhtml:p>
                            <xhtml:p>There are currently 20,000 meters being dialled using Meter Dialler.</xhtml:p>
                            <xhtml:p>The dialling of meters is retried up to 6 times, after which this retry limit needs to be reset.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li>A gas reading takes 3-4 minutes.</xhtml:li>
                                <xhtml:li>An electric reading takes 5-6 minutes.</xhtml:li>
                                <xhtml:li>A full meter reading would take 20 minutes.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Meter Dialler Database</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The Meter Dialler database (separate from the Meter Dialler app) essentially stores a work queue of message requests to contact the meters installed in the field to obtain up to date readings and usage information.</xhtml:p>
                            <xhtml:p>Refer to <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_DB_HL_MD_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Meter Dialler Database DevOps Help Guide" xhtml:alt="Meter Dialler Database DevOps Help Guide">Meter Dialler Database DevOps Help Guide</xhtml:a> for more information.</xhtml:p>
                            <xhtml:p>Each <xhtml:a xhtml:href="Sys_SMETS.htm" xhtml:target="_blank" xhtml:title="Smart Metering Equipment Technical Specification (SMETS)" xhtml:alt="Smart Metering Equipment Technical Specification (SMETS)">SMETS1</xhtml:a> smart meter installed by Utilita contains a SIM card which Meter Dialler uses to communicate with. Newer meters accept connections over the 3G network and so are contacted by SMS rather than being called using a modem. Older meters use GSM, but connection issues can occur with modem dialup, which is part of the reason for the move towards smart meters. Regardless of the age of the meter or the communication technology, readings are taken from each meter on a monthly or twice monthly basis to pull data which is then stored in the Meter Dialler DB.</xhtml:p>
                            <xhtml:p>The Meter Dialler DB maintains work queues for modem dialup, SMS and email communication with the appropriate meter population. As well as holding the requests to contact each meter, the delivery status of the messages are also written to the DB.</xhtml:p>
                            <xhtml:p>The SIM cards in the meters are from a range of mobile providers. The modems triggered by the Meter Dialler call queue are differently configured per SIM card provider, so designated O2 modems will contact O2 SIMs, Vodafone modems will contact Vodafone SIMs, and so on.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>