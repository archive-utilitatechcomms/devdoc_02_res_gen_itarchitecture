﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>CRM</MicroContentPhrase>
        <MicroContentPhrase>Customer Relationship Management</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>System: Customer Relationship Management (CRM)</xhtml:h1>
                    <xhtml:p>CRM is the main application used by call centre staff and it gives a view of all information held relating to the customer and their account. You can find further information about CRM in the following links:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_HL_CRM_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="CRM&#160;DevOps Help guide" xhtml:alt="CRM&#160;DevOps Help guide" xhtml:target="_blank">CRM&#160;DevOps Help guide</xhtml:a>
                        </xhtml:li>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/TechPubs/Help_CRM/Content/Topics/Welcome.htm" xhtml:title="CRM End User Help Guide" xhtml:alt="CRM End User Help Guide" xhtml:target="_blank">CRM End User Help Guide</xhtml:a>
                        </xhtml:li>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_HL_CRM_Help_REL/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="CRM Release Information" xhtml:alt="CRM Release Information" xhtml:target="_blank">CRM Release Information<xhtml:br /></xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                    <xhtml:p>Users are able to search for customers on a range of possible criteria which returns the full details of that customer. This includes their payment plan, contract details, agreements, supply start date, status, any additional account holders (can be multiples in a single location), additional notes (such as job bookings in <xhtml:a xhtml:href="00_JBS.htm" xhtml:title="Job Booking System (JBS) " xhtml:alt="Job Booking System (JBS) ">JBS</xhtml:a>) and so on.</xhtml:p>
                    <xhtml:p>Once the details have been returned, the user can drill down into any of the details to find out more detailed information. They are able to make various updates on the customer account records.</xhtml:p>
                    <xhtml:p>The Registrations team run a process every day to generate a list of new electricity customers, and a corresponding list of new gas customers. These get sent to the relevant industry regulator (Electralink or Xoserve respectively).</xhtml:p>
                    <xhtml:p>A record of all the system messages written to the DB when details are sent and received during the sign up and registration process is available by drilling down into CRM. This provides a full audit history of the process and actions for every customer. This is then maintained on an on-going basis for meter readings, top ups, etc.</xhtml:p>
                    <xhtml:p>CRM also contains meter details, including an audit of historic readings from the date the customer went live with Utilita onwards. This information is taken from the <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_00_Intra_Gen_PageFront_WEB/Content/Topics/00_Global/DB_B2B.htm" xhtml:title="Web Service Engine (WSE)" xhtml:alt="Web Service Engine (WSE)" xhtml:target="_blank">Web Service Engine (WSE)</xhtml:a>.</xhtml:p>
                    <xhtml:p>The calorific value on gas meters is updated on a nightly basis to reflect pressure variations. This impacts on billing as the price can change for each customer.</xhtml:p>
                    <xhtml:p>
                        <xhtml:img xhtml:src="00_IMG_Arch_CRM.png" xhtml:class="Screen_Thumbnail" />
                    </xhtml:p>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>