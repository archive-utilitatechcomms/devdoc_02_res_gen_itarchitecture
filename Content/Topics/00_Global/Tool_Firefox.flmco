﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>Mozilla Firefox</MicroContentPhrase>
        <MicroContentPhrase>Firefox</MicroContentPhrase>
        <MicroContentPhrase>Browser</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1><xhtml:a xhtml:name="Google_Chrome"></xhtml:a>Tool: Mozilla Firefox (Browser)</xhtml:h1>
                    <xhtml:p><xhtml:a xhtml:href="https://www.mozilla.org/en-US/firefox/new/?utm_medium=referral&amp;utm_source=firefox-com" xhtml:target="_blank" xhtml:title="Mozilla Firefox" xhtml:alt="Mozilla Firefox">Mozilla Firefox</xhtml:a> is a free and open-source web browser developed by <xhtml:a xhtml:href="https://foundation.mozilla.org/" xhtml:target="_blank" xhtml:title="Mozilla Foundation" xhtml:alt="Mozilla Foundation">Mozilla Foundation</xhtml:a> and its subsidiary, <xhtml:a xhtml:href="https://www.mozilla.org/en-US/" xhtml:target="_blank" xhtml:title="Mozilla Corporation" xhtml:alt="Mozilla Corporation">Mozilla Corporation</xhtml:a>. Firefox is available for Windows, macOS, Linux, and <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Berkeley_Software_Distribution" xhtml:target="_blank">BSD</xhtml:a> operating systems. </xhtml:p>
                    <xhtml:p>Its sibling, <xhtml:a xhtml:href="https://wiki.mozilla.org/Mobile" xhtml:target="_blank" xhtml:title="Firefox for Android" xhtml:alt="Firefox for Android">Firefox for Android</xhtml:a>, is available for <xhtml:a xhtml:href="https://www.android.com/" xhtml:target="_blank" xhtml:title="Android" xhtml:alt="Android">Android</xhtml:a>. Firefox uses the <xhtml:a xhtml:href="https://developer.mozilla.org/en-US/docs/Mozilla/Gecko" xhtml:target="_blank" xhtml:title="Gecko " xhtml:alt="Gecko ">Gecko layout engine</xhtml:a> to render web pages, which implements current and anticipated <xhtml:a xhtml:href="https://en.wikipedia.org/wiki/Web_standards" xhtml:target="_blank" xhtml:title="Web Standards" xhtml:alt="Web Standards">web standards</xhtml:a>. An additional version, <xhtml:a xhtml:href="https://www.mozilla.org/en-US/firefox/mobile/" xhtml:target="_blank" xhtml:title="Firefox for iOS" xhtml:alt="Firefox for iOS">Firefox for iOS</xhtml:a>, was released in late 2015; due to platform restrictions, it uses the <xhtml:a xhtml:href="https://webkit.org/" xhtml:target="_blank" xhtml:title="WebKit layout" xhtml:alt="WebKit layout">WebKit layout</xhtml:a> engine instead of Gecko, as with all other iOS web browsers.</xhtml:p>
                    <xhtml:p>You can use the <xhtml:a xhtml:href="https://developer.mozilla.org/son/docs/Tools" xhtml:target="_blank" xhtml:title="Firefox Developer Tools" xhtml:alt="Firefox Developer Tools">Firefox developer tools</xhtml:a> to examine, edit, and debug HTML, CSS, and JavaScript on the desktop and on mobile</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Creating</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>For authoring tools for websites and web apps.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li><xhtml:b>Scratchpad:</xhtml:b> A text editor built into Firefox that lets you write and execute JavaScript.</xhtml:li>
                                <xhtml:li><xhtml:b>Style Editor:</xhtml:b> View and edit CSS styles for the current page.</xhtml:li>
                                <xhtml:li><xhtml:b>Shader Editor:</xhtml:b> View and edit the vertex and fragment shaders used by WebGL.</xhtml:li>
                                <xhtml:li><xhtml:b>Web Audio Editor:</xhtml:b> Examine the graph of audio nodes in an audio context, and modify their parameters.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Exploring and debugging</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Examine, explore, and debug websites and web apps.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Web Console:</xhtml:b> See messages logged a web page, and interact with the page using JavaScript.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Page Inspector:</xhtml:b> View and modify the page HTML and CSS.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>JavaScript Debugger:</xhtml:b> Stop, step through, examine and modify the JavaScript running in a page.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Network Monitor:</xhtml:b> See the network requests made when a page is loaded.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Storage Inspector:</xhtml:b> Inspect cookies, local storage, indexedDB and session storage present in a page.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Developer Toolbar:</xhtml:b> A command-line interface for the developer tools.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>3D View:</xhtml:b> 3D visualization of the page.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Eyedropper:</xhtml:b> Select a color from the page.</xhtml:li>
                                <xhtml:li xhtml:style="font-weight: normal;"><xhtml:b>Working with iframes:</xhtml:b> How to target a particular iframe.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Mobile</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Tools for mobile development.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li><xhtml:b>App Manager:</xhtml:b> App Manager has been replaced by WebIDE.</xhtml:li>
                                <xhtml:li><xhtml:b>WebIDE:</xhtml:b> The replacement for the App Manager, available from Firefox 33 onwards.</xhtml:li>
                                <xhtml:li><xhtml:b>Firefox OS Simulator:</xhtml:b> Run and debug your Firefox OS app on the desktop, without needing a real Firefox OS device.</xhtml:li>
                                <xhtml:li><xhtml:b>Responsive Design View:</xhtml:b> See how your website or app will look on different screen sizes without changing the size of your browser window.</xhtml:li>
                                <xhtml:li><xhtml:b>Debugging on Firefox for Android:</xhtml:b> Connect the developer tools to Firefox for Android.</xhtml:li>
                                <xhtml:li><xhtml:b>Debugging Firefox for Android with WebIDE:</xhtml:b> For Desktop Firefox 36+ / Android Firefox 35+, there's a simpler process.</xhtml:li>
                                <xhtml:li><xhtml:b>Valence:</xhtml:b> Connect the developer tools to Chrome on Android and Safari on iOS.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Performance</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Diagnose and fix performance problems.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li><xhtml:b>Performance tool:</xhtml:b> Analyze your site's general responsiveness, JavaScript and layout performance.</xhtml:li>
                                <xhtml:li><xhtml:b>Frame rate graph:</xhtml:b> See the frame rate for your site.</xhtml:li>
                                <xhtml:li><xhtml:b>Waterfall:</xhtml:b> Figure out what the browser is doing as it runs your site.</xhtml:li>
                                <xhtml:li><xhtml:b>Call Tree:</xhtml:b> Figure out where your JavaScript code is spending its time.</xhtml:li>
                                <xhtml:li><xhtml:b>Flame Chart:</xhtml:b> See which functions are on the stack over the course of a performance profile.</xhtml:li>
                                <xhtml:li><xhtml:b>Paint Flashing Tool:</xhtml:b> Highlights the parts of the page that are repainted in response to events.</xhtml:li>
                                <xhtml:li><xhtml:b>Reflow Event Logging:</xhtml:b> See reflow events in the web console.</xhtml:li>
                                <xhtml:li><xhtml:b>Network Performance:</xhtml:b> See how long the parts of your site take to load.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Debugging the browser</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>By default, the developer tools are attached to a web page or web app. But you can also connect them to the browser as a whole. This is useful for browser and add-on development.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li><xhtml:b>Browser Console:</xhtml:b> See messages logged by the browser itself and add-ons, and run JavaScript code in the browser's scope.</xhtml:li>
                                <xhtml:li><xhtml:b>Browser Toolbox:</xhtml:b> Attach the Developer Tools to the browser itself.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Extending the devtools</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The developer tools are designed to be extensible. Firefox add-ons can access the developer tools and the components they use to extend existing tools and add new tools. With the remote debugging protocol you can implement your own debugging clients and servers, enabling you to debug websites using your own tools or to debug different targets using the Firefox tools.</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li><xhtml:b>Add a new panel to the devtools:</xhtml:b> Write an add-on that adds a new panel to the Toolbox.</xhtml:li>
                                <xhtml:li><xhtml:b>Remote Debugging Protocol:</xhtml:b> The protocol used to connect the Firefox Developer Tools to a debugging target like an instance of Firefox or a Firefox OS device.</xhtml:li>
                                <xhtml:li><xhtml:b>Source Editor:</xhtml:b> A code editor built into Firefox that can be embedded in your add-on.</xhtml:li>
                                <xhtml:li><xhtml:b>The Debugger Interface:</xhtml:b> An API that lets JavaScript code observe the execution of other JavaScript code. The Firefox Developer Tools use this API to implement the JavaScript debugger.</xhtml:li>
                                <xhtml:li><xhtml:b>Web Console custom output:</xhtml:b> How to extend and customize the output of the Web Console and the Browser Console.</xhtml:li>
                                <xhtml:li><xhtml:b>Example devtools add-ons:</xhtml:b> Use these examples to understand how to implement a devtools add-on.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>More resources</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:ul>
                                <xhtml:li><xhtml:b>Firebug:</xhtml:b> A very popular and powerful web development tool, including a JavaScript debugger, HTML and CSS viewer and editor, and network monitor.</xhtml:li>
                                <xhtml:li><xhtml:b>DOM Inspector: Inspect,</xhtml:b> browse, and edit the DOM of web pages or XUL windows.</xhtml:li>
                                <xhtml:li><xhtml:b>Web Developer:</xhtml:b> Adds a menu and a toolbar to the browser with various web developer tools.</xhtml:li>
                                <xhtml:li><xhtml:b>Webmaker Tools:</xhtml:b> A set of tools developed by Mozilla, aimed at people getting started with Web development.</xhtml:li>
                                <xhtml:li><xhtml:b>W3C Validators:</xhtml:b> The W3C website hosts a number of tools to check the validity of your website, including its HTML and CSS.</xhtml:li>
                                <xhtml:li><xhtml:b>JSHint:</xhtml:b> JavaScript code analysis tool.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>