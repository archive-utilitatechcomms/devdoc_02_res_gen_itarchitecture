﻿<?xml version="1.0" encoding="utf-8"?>
<MicroContentSet Comment="Default micro content set.">
    <MicroContent>
        <MicroContentPhrase>B2B&#160;Audit Tool</MicroContentPhrase>
        <MicroContentPhrase>Low Level App</MicroContentPhrase>
        <MicroContentPhrase>B2B Audit</MicroContentPhrase>
        <MicroContentPhrase>B2B</MicroContentPhrase>
        <MicroContentPhrase>SMSO</MicroContentPhrase>
        <MicroContentPhrase>Web Service Engine</MicroContentPhrase>
        <MicroContentPhrase>WSE</MicroContentPhrase>
        <MicroContentPhrase>HES</MicroContentPhrase>
        <MicroContentPhrase>Smart HES)</MicroContentPhrase>
        <MicroContentPhrase>Head End System</MicroContentPhrase>
        <MicroContentPhrase>Staging</MicroContentPhrase>
        <MicroContentPhrase>Staging Database</MicroContentPhrase>
        <MicroContentPhrase>BtoB</MicroContentPhrase>
        <MicroContentPhrase>Smart Metering System</MicroContentPhrase>
        <MicroContentPhrase>Smart Metering</MicroContentPhrase>
        <MicroContentPhrase>Smart Metering System Operator</MicroContentPhrase>
        <MicroContentPhrase>Smart Staging Database</MicroContentPhrase>
        <MicroContentResponse>
            <xhtml:html xmlns:MadCap="http://www.madcapsoftware.com/Schemas/MadCap.xsd" xmlns:xhtml="http://www.w3.org/1999/xhtml">
                <xhtml:body>
                    <xhtml:h1>Database: Staging&#160;(SMSO / B2B / WSE / HES)</xhtml:h1>
                    <xhtml:p>Utilita’s Smart Meter network includes B2B components that Secure (Secure Meters Limited) previously developed for Utilita.</xhtml:p>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Business-to-Business (B2B) or Smart Meter System Operator (SMSO)</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>Also known as the Business-to-Business (B2B) or Smart Meter System Operator (SMSO).</xhtml:p>
                            <xhtml:ul>
                                <xhtml:li> The Smart Metering System (SMSO)  is comprised of the system components required to deliver the smart functionality, meter, WAN, HAN, and an IHD (where provided). </xhtml:li>
                                <xhtml:li>The system has the ability to remotely read non-half hourly (NHH) meters, and the data produced by SMSO helps to produce more reliable and accurate bills.</xhtml:li>
                            </xhtml:ul>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>Web Service Engine (WSE)</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The WSE is the web services interface, controlled by Secure, that manages data into and out of SMETS meters for meter management.</xhtml:p>
                            <xhtml:p>Part of the meter messaging element of the system, the WSE sits between the B2B application and the SMETS meter population. It contains payment card information as well as meter details and shares a direct link with PayPoint. When a customer tops up, the information is recorded in the WSE. The WSE communicates with both the customer's meter to add the credit, and with PayPoint to register the top up to add to the customer's records.</xhtml:p>
                            <xhtml:p>PayPoint sends a file of daily transactions from customers back to Utilita. This information is also available from the WSE which sends credit transactions to be loaded into the Production database.</xhtml:p>
                            <xhtml:p>The PayPoint file also contains details of the payment method used by the customer, the postcode if the top up was paid for in a physical store, customer details where a receipt has been requested, and so on. This information is not included in the communications sent from the WSE.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <MadCap:dropDown>
                        <MadCap:dropDownHead>
                            <MadCap:dropDownHotspot>The Staging Database (HES)</MadCap:dropDownHotspot>
                        </MadCap:dropDownHead>
                        <MadCap:dropDownBody>
                            <xhtml:p>The Staging database (or Smart Staging database), also know as the Head End System (HES) is Utilita's production database that holds all messages to and from <xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_00_Intra_Gen_PageFront_WEB/Content/Topics/00_Global/Sys_SMETS.htm" xhtml:target="_blank" xhtml:title="System - Smart Metering Equipment Technical Specification (SMETS)" xhtml:alt="System - Smart Metering Equipment Technical Specification (SMETS)">SMETS</xhtml:a> smart meters. This is a client system utilising SMSO web services - Utilitia is defined as the client (user) of the head-end system of the web services exposed by the SMSO.</xhtml:p>
                            <xhtml:p>For instance, as part of the messaging element of the system, the B2B is an application developed by Secure that sits inside Utilita's infrastructure. It is the interface between Utilita's Staging database and Secure's Web Service Engine (WSE) which operates in real time to communicate with the SMETS meter population... Meters connect to the Web Service Engine (WSE) and an interface exists between this WSE and Utilita's production system that consist of a B2B application and the Staging database... Oracle requests are sent by the Staging database that are converted into web requests and sent on to the meters. Responses are returned and logged to the Staging database.</xhtml:p>
                            <xhtml:p xhtml:class="Note2" MadCap:autonum="&lt;b&gt;Note: &lt;/b&gt;">The physical SMETS meters are owned by the manufacturer / operator, not by Utilita, so exist outside of the company's controlled network. The Staging DB is hosted in Utilita's environment, even though it's accessed by third parties.</xhtml:p>
                            <xhtml:p>&#160;</xhtml:p>
                        </MadCap:dropDownBody>
                    </MadCap:dropDown>
                    <xhtml:p>Here's some additional information:</xhtml:p>
                    <xhtml:ul>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_HL_B2B_SMSO-HES_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:target="_blank" xhtml:title="Staging Database (Web Service Engine Database / WSE / HES / SMSO / B2B)" xhtml:alt="Staging Database (Web Service Engine Database / WSE / HES / SMSO / B2B)">B2B SMSO DevOps Help Guide</xhtml:a>
                        </xhtml:li>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_LL_B2B_AuditTool_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="B2B&#160;Audit Tool DevOps Help Guide" xhtml:alt="B2B&#160;Audit Tool DevOps Help Guide" xhtml:target="_blank">B2B&#160;Audit Tool DevOps Help Guide</xhtml:a>
                        </xhtml:li>
                        <xhtml:li><xhtml:a xhtml:href="http://ugl-jir-001-dev/DevDocs/DevDoc_03_Service_HL_B2B_SMSO-HES_Help_GDE/Content/Topics/01_Introduction/01_PageFront_01_Info_A.htm" xhtml:title="Staging&#160;Database&#160;DevOps Help guide" xhtml:alt="Staging&#160;Database&#160;DevOps Help guide" xhtml:target="_blank">Staging&#160;Database&#160;DevOps Help guide</xhtml:a>
                        </xhtml:li>
                    </xhtml:ul>
                </xhtml:body>
            </xhtml:html>
        </MicroContentResponse>
    </MicroContent>
</MicroContentSet>